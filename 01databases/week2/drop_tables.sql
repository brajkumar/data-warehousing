drop table CUSTOMER cascade constraints PURGE;
drop table FACILITY cascade constraints PURGE;
drop table LOCATION cascade constraints PURGE;
drop table EVENTREQUEST cascade constraints PURGE;
drop table EMPLOYEE cascade constraints PURGE;
drop table EVENTPLAN cascade constraints PURGE;
drop table RESOURCETBL cascade constraints PURGE;
drop table EVENTPLANLINE cascade constraints PURGE;

